function headerMenu() {
  var data1 = document.querySelector(".mobile-menu-icon");
  var data2 = document.querySelector(".link-sec");
  if (data2.style.display === "block") {
    data2.style.display = "none";
  } else {
    data2.style.display = "block";
  }
}

// CHANGE HEADER COLOR ON SCROLL
document.addEventListener("DOMContentLoaded", function () {
  var header = document.getElementById("header");
  console.log(header, "headerrrrrr");
  window.onscroll = function () {
    if (
      document.body.scrollTop > 550 ||
      document.documentElement.scrollTop > 550
    ) {
      header.style.backgroundColor =
        "#003b6d"; /* Change to the desired color on scroll */
    } else {
      header.style.backgroundColor =
        "rgb(51 51 51 / 35%)"; /* Initial color when not scrolled */
    }
  };
});
